# Navigator OPI Generator
This script is used for generating a Navigator OPI file resembling a tree-like structure menu.

## Requirements
The ESS file structure, defined in ESS-3149152, should be followed. A minimum requirement is the following:
- All launchable OPIs are to be located in a folder called 10-Top.
- Any embedded or non-launchable OPI, symbols etc. should be in another subdirectory (e.g. 99-Shared).
- Overview screens (e.g. a PBI or MEBT overview screen) should be named `overview.bob` if this OPI button should be merged into the section or system overview (expandable) node.

In order to properly portray all OPIs, the ESS StyleGuide should also be followed (at least for including a 50px high titlebar within all launchable OPIs in `10-Top`).

### Example
- All launchable OPIs in here:          `/Users/benjaminbolling/Phoebus/phoebus-opis/opis/30-Operator/10-ACC/999-PBI/10-Top/`
- All non-launchable objects in here:   `/Users/benjaminbolling/Phoebus/phoebus-opis/opis/30-Operator/10-ACC/999-PBI/99-Shared/`

## Usage Instructions
0. Clone the repository or download the two python files (*navigatorIO.py* and *constructNavigator.py*).
1. Launch a terminal from where the Python files are located.
2. Execute:   `python constructNavigator.py`.
3. Select the directory of the OPIs.
4. The GUI shown in Figure 0 appears. Select or deselect OPI files to include or exclude, respectively, from the OPI Navigator OPI using the left column's checkboxes. Column 3 (`Text`) shows the text that will appear on each button, which can be changed into any text (maximum length being 10 characters).
5. To build the Navigator, press the 'Create Navigator' button.
6. When finished, the text `Done! Navigator OPI created from OPIs present in: >>  /User/Path/Where/OPIs/location/were/selected` is shown in the terminal.
8. The *navigator.bob*-file has been created with support files in a new folder *opi* and can be launched using CS Studio Phoebus.

Note: The two Python files (*navigatorIO.py* and *constructNavigator.py*) can now be removed.

### Moving the Navigator OPI to another location
The Navigator OPI can be generated on a local computer (used for development), meaning that the OPIs are not in the same location as they are on the target computer. For this purpose, a macro `$()` is used which can be edited also after the Navigator OPI has been generated. The macro value can then be changed in CS Studio Phoebus (to e.g. /opt/opis/), see Figure 2.

## Figures
<table>
    <tr>
        <td>
            <img alt="builder" src="docs/builder.png">
        </td>
    </tr>
</table>
Figure 0: The GUI for building the OPI Navigator OPI.

<table>
    <tr>
        <td>
            <img alt="navigator opi" src="docs/navigator.png">
        </td>
    </tr>
</table>
Figure 1: An example screen of a generated Navigator OPI, showing the FC Operator Level OPI (/Users/benjaminbolling/Phoebus/phoebus-opis/opis/30-Operator/10-ACC/999-PBI/10-Top/FC.bob).

<table>
    <tr>
        <td>
            <img alt="macros" src="docs/macro.png">
        </td>
    </tr>
</table>
Figure 2: The macro that should be edited if the target computer's file structure differs from the development computer's.

<table>
    <tr>
        <td>
            <img alt="flowchart" src="docs/constructorFlowchart.png">
        </td>
    </tr>
</table>
Figure 3: The construction mechanism flowchart.
